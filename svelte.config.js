/* import autoprefixer from 'autoprefixer'; */
import preprocess from 'svelte-preprocess';
/* import tailwindcss from 'tailwindcss'; */
import dotenv from 'dotenv';
/* import path from 'path'; */
import adapter from '@sveltejs/adapter-node';
dotenv.config();

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
		adapter: adapter({ out: 'build' }),
		/* vite: {
			css: {
				postcss: {
					plugins: [tailwindcss(), autoprefixer()]
				}
			},
			define: {
				'process.env': process.env
			},
			resolve: {
				alias: {
					$media: path.resolve('./src/media')
				}
			}
		}, */
		// hydrate the <div id="svelte"> element in src/app.html
		/* target: '#svelte' */
		routes: (filepath) => {
			return ![
				// exclude *test.js files
				/\.test\.js$/,
				/\.test\.svelte$/,

				// original default config
				/(?:(?:^_|\/_)|(?:^\.|\/\.)(?!well-known))/
			].some((regex) => regex.test(filepath));
		}
	},
	preprocess: [
		preprocess({
			postcss: true
		})
	]
};

export default config;
