import { sveltekit } from '@sveltejs/kit/vite';
import tailwindcss from 'tailwindcss';
import autoprefixer from 'autoprefixer';
import path from 'path';

/** @type {import('vite').UserConfig} */
const config = {
	plugins: [sveltekit()],
	css: {
		postcss: {
			plugins: [tailwindcss(), autoprefixer()]
		}
	},
	define: {
		'process.env': process.env
	},
	resolve: {
		alias: {
			$media: path.resolve('./src/media')
		}
	}
};

export default config;
