# Krusty

Server Dashboard - Alpha (0.0.1)

Built with Sveltekit, Tailwind & MongoDB (Mongoose) 💫

## Features

- User roles and authentication
- Private and shared bookmarks
- Optional bookmark icons and descriptions
- Mobile friendly interface

## To be developed

- Multiple languages, both for frontend-backend
- Wireguard management integration
- Customisable UI

---

### License

MIT

PR, bugs, comments are welcome!
