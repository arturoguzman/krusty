import { writable /* get */ } from 'svelte/store';
/* import { getData } from '../actions/actions'; */

export const globalMessage = writable({ message: '', color: '' });
export const globalMessageStatus = writable(false);

export const linksStore = writable([]);
export const usersStore = writable([]);
export const serverMessage = writable('');
