/* import { production } from '../../vars';
import { v4 as uuid } from 'uuid';
import { User } from './mongodb';
import _, { map } from 'underscore'; */
import { Url } from '../api/mongodb';
/* import { parse } from 'ipaddr.js'; */

export const POST = async (request) => {
	if (request.url.searchParams.get('newUrl')) {
		try {
			if (request.locals.authenticated.valid === true) {
				const body = await request.request.json();
				const authUserId = request.locals.user._id;
				const checkUrl = await Url.findOne({ url: body.url });
				if (checkUrl === null) {
					const newUrl = await Url.create({
						name: body.name,
						url: body.url,
						description: body.description,
						open: false,
						icon: body.icon,
						policy: body.policy,
						userCreated: authUserId
					});
					console.log(newUrl);
					return {
						status: 200,
						body: {
							message: newUrl.name
						}
					};
				} else {
					if (body.policy === true) {
						const newUrl = await Url.create({
							name: body.name,
							url: body.url,
							description: body.description,
							open: false,
							icon: body.icon,
							policy: body.policy,
							userCreated: authUserId
						});
						return {
							status: 200,
							newUrl
						};
					} else {
						return {
							status: 409,
							body: {
								message: 'url already exists!'
							}
						};
					}
				}
			} else {
				return {
					code: 401,
					message: `You can't create shit, mate`
				};
			}
		} catch (err) {
			console.log(err);
			return {
				status: 500,
				body: {
					message: 'error :('
				}
			};
		}
	}
};

export const PATCH = async (request) => {
	if (request.url.searchParams.get('update')) {
		try {
			const body = (await request.request.json()) || '';
			const filter = { _id: request.url.searchParams.get('update') };
			const update = {
				name: body.name,
				url: body.url,
				description: body.description,
				icon: body.icon,
				open: body.open,
				policy: body.policy
			};
			if (request.locals.user.role === 'admin') {
				const url = await Url.findOneAndUpdate(filter, update, { new: true });
				return {
					status: 200,
					body: {
						message: `${url.name} has been updated!`
					}
				};
			} else {
				let filterUrl = await Url.where({ _id: request.url.searchParams.get('update') });
				if (filterUrl[0].userCreated) {
					const userCreated = filterUrl[0].userCreated.toString() || '';
					if (userCreated === request.locals.user._id.toString()) {
						let newFilter = { _id: request.url.searchParams.get('update') };
						const url = await Url.findOneAndUpdate(newFilter, update, { new: true });
						return {
							status: 200,
							body: {
								message: `${url.name} has been updated!`
							}
						};
					} else {
						return {
							status: 401,
							body: {
								message: `You can't update this!`
							}
						};
					}
				} else {
					return {
						status: 401,
						body: {
							message: `You can't update this!`
						}
					};
				}
			}
		} catch (err) {
			console.log(err);
			return {
				status: 401,
				body: {
					message: `Error updating ${request.url.searchParams.get('update')}! ${err}`
				}
			};
		}
	}
};

export const DELETE = async (request) => {
	if (request.url.searchParams.get('id')) {
		const id = request.url.searchParams.get('id');
		const name = request.url.searchParams.get('name');
		const deleted = await Url.findOneAndDelete({ _id: id });
		if (deleted !== null) {
			console.log(deleted);
			return {
				status: 200,
				body: {
					message: `${deleted.name} has been deleted!`
				}
			};
		} else {
			return {
				status: 404,
				body: {
					message: `${name} does not exist!`
				}
			};
		}
	}
};
