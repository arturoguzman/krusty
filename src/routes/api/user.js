/* import { production } from '../../vars'; */
import * as cookie from 'cookie';
import bcrypt from 'bcryptjs';
import { User } from './mongodb';
/* import { v4 as uuid } from 'uuid'; */

export const GET = async (event) => {
	if (event.url.searchParams.get('all')) {
		try {
			if (event.locals.user.role === 'admin') {
				let users = [];
				const getUsers = await User.find({});
				getUsers.forEach((data) => {
					const user = {
						id: data.id,
						username: data.username,
						role: data.role
					};
					users = [...users, user];
				});
				console.log(users);
				return {
					status: 200,
					body: users
				};
			} else {
				return {
					status: 401,
					body: {
						message: `You can't get shit, mate`
					}
				};
			}
		} catch (err) {
			console.log(err);
			return {
				status: 500,
				body: {
					message: err
				}
			};
		}
	} else {
		return {
			status: 401,
			body: {
				message: 'tf you looking for'
			}
		};
	}
};

export const PATCH = async (event) => {
	if (event.url.searchParams.get('update')) {
		try {
			const cookies = cookie.parse(event.request.headers.get('cookie'));
			const checkUser = await User.find({ id: cookies.userid });
			if (checkUser !== null) {
				const body = (await event.request.json()) || '';
				let newBody = {};
				if (body.username) {
					if (checkUser[0].username === body.username) {
						console.log(`Won't update username, it is the same one!`);
					} else {
						const checkExistingUsername = await User.find({ username: body.username });
						if (checkExistingUsername.length > 0) {
							if (checkExistingUsername[0].id !== cookies.userid) {
								return {
									status: 404,
									body: {
										message: `Can't update username, requested username already exists!`
									}
								};
							}
						} else if (checkExistingUsername.length === 0) {
							console.log('will update username :)');
							newBody['username'] = body.username;
						}
					}
				}
				if (body.password) {
					const salt = bcrypt.genSaltSync(10);
					const hash = bcrypt.hashSync(body.password, salt);
					newBody['password'] = hash;
				}
				await checkUser[0].update(newBody);
				const reloadUser = await User.find({ id: cookies.userid });
				return {
					status: 200,
					body: {
						username: reloadUser[0].username,
						language: reloadUser[0].language
					}
				};
			} else {
				return {
					status: 404,
					body: {
						message: 'user not found!'
					}
				};
			}
		} catch (err) {
			console.log('this is error:', err);
			return {
				status: 500,
				body: {
					message: err
				}
			};
		}
	} else {
		console.log('nanana');
	}
};
/* export const del = async (request) => {
	if (request.url.searchParams.get('delete')) {
	}
};
 */

export const DELETE = async (request) => {
	if (request.url.searchParams.get('delete')) {
		const deleteUser = await User.findOneAndDelete({ id: request.url.searchParams.get('delete') });
		if (deleteUser) {
			return {
				status: 200,
				body: {
					message: `${deleteUser.username} has been deleted!`
				}
			};
		}
		return {
			status: 500,
			body: {
				message: 'There has been an error! :('
			}
		};
	}
};
