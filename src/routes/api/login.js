/* import { production } from '../../vars'; */
import bcrypt from 'bcryptjs';
import * as cookie from 'cookie';
import { User } from './mongodb';
/* import { v4 as uuid } from 'uuid'; */

export const POST = async (request) => {
	if (request.url.searchParams.get('bye')) {
		return {
			status: 200,
			headers: new Headers({
				'set-cookie': [
					'session_id=deleted; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT',
					'userid=deleted; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT'
				]
			}),
			body: {
				message: 'logged out!'
			}
		};
	} else if (request.url.searchParams.get('login')) {
		try {
			const body = (await request.request.json()) || '';
			if (!body.password || !body.username) {
				return {
					status: 400,
					body: {
						message: 'Empty fields...'
					}
				};
			}
			const password = body.password;
			const user = await User.find({ username: body.username });
			if (user.length > 0) {
				const dbpassword = user[0].password;
				let headers = {};
				let hashed = new Promise((resolve, reject) => {
					const result = bcrypt.compareSync(password, dbpassword);
					if (!result) {
						resolve({ result, code: 401 });
					} else if (result) {
						resolve({ result, code: 200, cookie: user[0].cookie });
					} else {
						reject();
					}
				});
				const result = await hashed;
				if (result.code === 200) {
					headers = new Headers({
						user: user[0].id,
						loggedIn: true
					});
					headers.append(
						'Set-Cookie',
						cookie.serialize('session_id', result.cookie, {
							httpOnly: true,
							maxAge: 60 * 60 * 24,
							sameSite: 'lax',
							path: '/'
						})
					);
					headers.append(
						'Set-Cookie',
						cookie.serialize('userid', user[0].id, {
							httpOnly: true,
							maxAge: 60 * 60 * 24,
							sameSite: 'lax',
							path: '/'
						})
					);
					return {
						status: result.code,
						headers,
						body: {
							username: user[0].username,
							role: user[0].role,
							language: user[0].language || 'default'
						}
					};
				} else {
					return {
						status: 404,
						body: {
							message: 'Error logging in! .('
						}
					};
				}
			} else {
				return {
					status: 404,
					body: {
						message: 'User not found .('
					}
				};
			}
		} catch (err) {
			console.log(err);
			return {
				status: 500,
				body: {
					message: 'Error logging in :('
				}
			};
		}
	}
};
