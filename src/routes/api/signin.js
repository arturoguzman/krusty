/* import { production } from '../../vars'; */
/* import * as cookie from 'cookie'; */
import bcrypt from 'bcryptjs';
import { User } from './mongodb';
import { v4 as uuid } from 'uuid';

export const POST = async (request) => {
	if (request.url.searchParams.get('create')) {
		try {
			if (
				(request.locals.authenticated.valid === true && request.locals.user.role === 'admin') ||
				request.locals.allowSignin
			) {
				const body = (await request.request.json()) || '';
				if (!body.username || !body.password) {
					return {
						status: 400,
						body: {
							error: 'Empty fields!'
						}
					};
				}
				const checkUser = await User.findOne({ username: body.username });
				if (checkUser === null) {
					let hashed = new Promise((resolve, reject) => {
						try {
							let userCreated = {};
							const salt = bcrypt.genSaltSync(10);
							const hash = bcrypt.hashSync(body.password, salt);
							const save = async (hash) => {
								userCreated = await User.create({
									id: uuid(),
									username: body.username,
									password: hash,
									cookie: uuid(),
									language: 'en',
									role: body.role
								});
								resolve({
									id: userCreated.id,
									username: userCreated.username,
									cookie: userCreated.cookie,
									role: userCreated.role
								});
							};
							save(hash);
						} catch (err) {
							console.log(err);
							reject(err);
						}
					});
					return {
						status: 200,
						body: {
							message: await hashed
						}
					};
				} else {
					return {
						status: 409,
						body: {
							message: 'Username already exists!'
						}
					};
				}
			} else {
				return {
					status: 401,
					body: {
						message: `You can't create shit, mate`
					}
				};
			}
		} catch (err) {
			console.log(err);
			return {
				status: 500,
				body: {
					message: 'Server error :('
				}
			};
		}
	}
};
