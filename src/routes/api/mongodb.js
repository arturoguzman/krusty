import mongoose from 'mongoose';
import { production } from '../../vars';

const dbUrl = production.mongodb_url;
const dbName = production.mongodb_name;
const dbOptions = production.mongodb_options;

const url = dbUrl + dbName + dbOptions;

export const database = async () => {
	await mongoose.connect(url);
};

const userSchema = new mongoose.Schema({
	id: String,
	username: String,
	userType: String,
	password: String,
	cookie: String,
	role: String,
	customUrl: {
		type: mongoose.SchemaTypes.ObjectId,
		ref: 'Url'
	}
});

export const User = mongoose.model('User', userSchema);

const urlSchema = new mongoose.Schema({
	name: String,
	url: String,
	description: String,
	icon: String,
	open: { type: Boolean, default: false },
	policy: { type: Boolean, default: false },
	userCreated: {
		type: mongoose.SchemaTypes.ObjectId,
		ref: 'User'
	}
});

export const Url = mongoose.model('Url', urlSchema);

const messageSchema = new mongoose.Schema({
	message: String,
	id: String
});

export const Message = mongoose.model('Message', messageSchema);
