import { Url, Message } from '../api/mongodb';

export const GET = async (request) => {
	if (request.url.searchParams.get('url')) {
		let getUrls, getPrivateUrls;
		getUrls = await Url.where({ policy: false });
		if (request.locals.authenticated.valid === true) {
			getPrivateUrls = await Url.where({ policy: true }).where({
				userCreated: request.locals.user._id
			});
			for (const privateUrl of getPrivateUrls) {
				getUrls = [...getUrls, privateUrl];
			}
		}
		let message = await Message.find({ id: 'thisismyunnecessarlylongcustomid' });
		return {
			status: 200,
			body: { message: message[0].message, urls: getUrls }
		};
	} else {
		return {
			status: 500,
			body: {
				message: 'error :('
			}
		};
	}
};

export const POST = async (request) => {
	if (request.url.searchParams.get('message')) {
		const body = (await request.request.json()) || '';
		let message = await Message.find({ id: 'thisismyunnecessarlylongcustomid' });
		if (message.length === 0)
			message = await Message.create({ id: 'thisismyunnecessarlylongcustomid', message: body });
		const updatedMessage = await Message.findOneAndUpdate(
			{ id: 'thisismyunnecessarlylongcustomid' },
			{ message: body }
		);
		return {
			status: 200,
			body: updatedMessage
		};
	} else {
		return {
			status: 500,
			body: {
				message: 'error :('
			}
		};
	}
};
