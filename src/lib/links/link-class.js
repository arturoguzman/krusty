export default class Url {
	constructor(_name, _url, _description, _open, _id, _icon, _policy, _userCreated) {
		this.name = _name;
		this.url = _url || 'http://';
		this.description = _description;
		this.open = _open;
		this.id = _id;
		this.icon = _icon || '';
		this.policy = _policy;
		this.userCreated = _userCreated;
	}
	async create() {
		const body = {
			name: this.name,
			url: this.url,
			description: this.description,
			icon: this.icon,
			open: this.open,
			policy: this.policy
		};
		const res = await fetch('/api/links?newUrl=test', {
			method: 'POST',
			body: JSON.stringify(body)
		});
		const data = await res.json();
		return {
			status: res.status,
			message: data.message
		};
	}

	async status() {
		try {
			const body = {
				id: this.id,
				open: this.open
			};
			const res = await fetch(`/api/links?update=${body.id}`, {
				method: 'PATCH',
				body: JSON.stringify(body)
			});
			const data = await res.json();
			return {
				status: res.status,
				message: data.message
			};
		} catch (err) {
			console.error(err);
		}
	}

	async save() {
		const body = {
			id: this.id,
			name: this.name,
			url: this.url,
			description: this.description,
			icon: this.icon,
			open: this.open,
			policy: this.policy
		};
		const res = await fetch(`/api/links?update=${body.id}`, {
			method: 'PATCH',
			body: JSON.stringify(body)
		});
		const data = await res.json();
		return {
			status: res.status,
			message: data.message
		};
	}

	async delete(link) {
		const res = await fetch(`/api/links?name=${link.name}&id=${link.id}`, {
			method: 'DELETE'
		});
		const data = await res.json();
		console.log(data);
		return {
			status: res.status,
			message: data.message
		};
	}
}
