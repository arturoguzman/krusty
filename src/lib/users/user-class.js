export default class User {
	constructor(_username, _password, _id, _role, _language) {
		this.id = _id;
		this.username = _username;
		this.password = _password;
		this.role = _role;
		this.language = _language;
	}
	async create() {
		const body = {
			username: this.username,
			password: this.password,
			role: this.role
		};
		const res = await fetch(`/api/signin?create=${body.username}`, {
			method: 'POST',
			body: JSON.stringify(body)
		});
		const data = await res.json();
		console.log(data);
		/* if (res.ok) { */
		return {
			status: res.status,
			message: data.message.username
		};
		/* } */
	}

	async login() {
		const body = {
			username: this.username,
			password: this.password
		};
		if (!body.username || !body.password) {
			return false;
		}
		const res = await fetch(`/api/login?login=${body.username}`, {
			method: 'POST',
			body: JSON.stringify(body)
		});
		const data = await res.json();
		console.log('this is res status:', res.status);
		if (res.status === 200) {
			this.username = data.username;
			this.role = data.role;
			this.language = data.language;
			return true;
		} else {
			return false;
		}
	}

	async save() {
		const body = {
			id: this.id,
			username: this.username,
			password: this.password
		};
		const res = await fetch(`/api/user?update=${body.id}`, {
			method: 'PATCH',
			body: JSON.stringify(body)
		});
		const data = await res.json();
		if (res.status === 200) {
			this.username = data.username;
			this.language = data.language || 'default';
			return true;
		} else {
			return false;
		}
	}

	async delete() {
		const res = await fetch(`/api/user?delete=${this.id}`, {
			method: 'DELETE'
		});
		const data = await res.json();
		console.log(data);
		/* 		if (res.ok) {
			return true;
		} */
		return {
			status: res.status,
			message: data.message
		};
	}
}
