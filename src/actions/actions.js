import { globalMessage, globalMessageStatus } from '../stores/store';

export const setGlobalMessage = async (message, color, time) => {
	function clearGlobalMessage() {
		globalMessage.set({ message: '', color: '' });
		globalMessageStatus.set(false);
	}
	try {
		let duration = '5000';
		if (time) {
			duration = time;
		}
		globalMessageStatus.set(true);
		globalMessage.set({ message: message, color: color });
		setTimeout(clearGlobalMessage, duration);
	} catch (error) {
		console.log(error);
	}
};
