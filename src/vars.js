export const production = {
	mongodb_url: '',
	mongodb_name: '',
	mongodb_options: '',
	directus_auth: '',
	images_upload: '',
	admin_account: '',
	images_download: '',
	testing: '',
	languageSelection: '',
	allowSignin: ''
};

if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'preview') {
	// For production/preview & Vercel
	production.mongodb_url = process.env.MONGODB_URL;
	production.mongodb_name = process.env.MONGODB_NAME;
	production.mongodb_options = process.env.MONGODB_OPTIONS;
	production.directus_auth = process.env.DIRECTUS_AUTH;
	production.admin_account = process.env.ADMIN_ACCOUNT;
	production.images_upload = process.env.IMAGES_URL;
	production.images_download = process.env.IMAGES_DOWNLOAD_URL;
	production.languageSelection = process.env.LANGUAGE;
	production.allowSignin = process.env.ALLOWUSERCREATE;
} else {
	// For development
	production.mongodb_url = import.meta.env.VITE_MONGODB_URL;
	production.mongodb_name = import.meta.env.VITE_MONGODB_NAME;
	production.mongodb_options = import.meta.env.VITE_MONGODB_OPTIONS;
	production.directus_auth = import.meta.env.VITE_DIRECTUS_AUTH;
	production.admin_account = import.meta.env.VITE_ADMIN_ACCOUNT;
	production.images_upload = import.meta.env.VITE_IMAGES_URL;
	production.images_download = import.meta.env.VITE_IMAGES_DOWNLOAD_URL;
	production.languageSelection = import.meta.env.VITE_LANGUAGE;
	production.allowSignin = import.meta.env.VITE_ALLOWUSERCREATE;
}
