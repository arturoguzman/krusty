import * as cookie from 'cookie';
import { production } from './vars';
/* import dotenv from 'dotenv';
dotenv.config(); */
/* import { production } from './vars'; */
import { User, database } from './routes/api/mongodb';

const verifyToken = async (token, db) => {
	if (token === db) {
		return true;
	} else {
		return false;
	}
};

let activeUser = {};
let isLoggedIn = false;
let username = '';
export async function handle({ event, resolve }) {
	database();
	event.locals.user = '';
	console.log(`

	Krusty – Server Homepage alpha version :)
	🏁 New event initiated
	
	`);
	let publicPages = ['/', '/api/data', '/api/login', '/api/test'];
	const allowedMethods = ['GET', 'POST'];
	// who is this?
	const cookies = cookie.parse(event.request.headers.get('cookie') || '');
	// if cookie
	if (cookies.session_id) {
		console.log('🍪 cookies session exists!');
		const findUser = await User.find({ id: cookies.userid });
		const resultUser = findUser[0];
		isLoggedIn = await verifyToken(cookies.session_id, resultUser.cookie);
		if (isLoggedIn) {
			activeUser.guest = false;
			activeUser.username = resultUser.username;
			activeUser.language = resultUser.language;
			activeUser.role = resultUser.role;
			activeUser.id = resultUser.id;
			activeUser._id = resultUser._id;
			event.locals.user = activeUser;
			event.locals.authenticated = {
				valid: true
			};
		} else {
			return {
				status: 403,
				response: {
					message: 'what are you trying to doooo? who are you?!'
				}
			};
		}
	} else {
		if (production.allowSignin) {
			console.log(
				`'allowSignin' is enabled – This configuration will allow new users to be created! Be careful! 👀`
			);
			event.locals.allowSignin;
			username = 'allowSignin';
			event.locals.authenticated = {
				valid: false
			};
			isLoggedIn = false;
		} else {
			event.locals.authenticated = {
				valid: false
			};
			isLoggedIn = false;
		}
	}
	console.log('🙊 Logged in?', isLoggedIn);
	if (!isLoggedIn && !allowedMethods.includes(event.request.method)) {
		const invalidResponse = new Response(
			{ message: 'what are you trying to doooo?!' },
			{ status: 403 }
		);
		return invalidResponse;
	}
	if (!isLoggedIn && username !== 'allowSignin' && !publicPages.includes(event.url.pathname)) {
		const invalidResponse = new Response(
			{ message: 'oop' },
			{ status: 302, headers: { location: '/' } }
		);
		return invalidResponse;
	}
	const response = await resolve(event);
	console.log(`
	
	🔚 Request completed!
    	–––––––––––––––––––––
	
	`);

	return response;
}

export function getSession(event) {
	console.log('🤓 session set:', event.locals.user.username);
	return isLoggedIn
		? {
				user: {
					guest: false,
					username: event.locals.user.username,
					language: event.locals.user.language,
					role: event.locals.user.role,
					id: event.locals.user._id.toString()
				}
		  }
		: {
				user: {
					guest: true,
					username,
					language: 'default'
				}
		  };
}
